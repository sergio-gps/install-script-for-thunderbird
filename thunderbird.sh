#!/bin/bash
#
# This script will automagically download and install the thunderbird bundle from Mozilla.
# After that it will restore alternatives (Debian, Ubuntu or derivatives) and symlinks.
#
# Run with sudo for a system-wide installation.
# 1. run without any parameters to download and install with confirmation.
# 2. pass parameter: -q in order to query whether an update is available.
# 3. pass parameter: -s in order to restore symlinks only.
# 4. pass parameter: -y in order to auto-upgrade when a new version is found.
# 5. pass parameter: -x to uninstall the app
#
# Depends on: sed, awk, runuser, tar, sleep, egrep, getconf, curl, wget, sha512sum, test
#
# For integration with updates detection and 'apt',
# deploy it to: /usr/local/sbin/
#

appname=thunderbird
appname_proper=$(echo ${appname} | sed -E 's/^(.)(.*)$/\u\1\2/')

allargs=("$@")
numargs=$#

doUninstall=false
optQueryUpdate=false
optRestoreSymlinks=false
optAutoUpgrade=false
exitStatusSuccess=0
exitStatusFail=1
exitStatusUpdateAvailable=100
deleteToTrash=false

systemWide=
shortcutDir=
installPath=/opt
installDesktopDir=
installIconDir=
iconName=${appname}.png
iconFile=
appInstallDir=
shortcutFile=
desktopFile=

iconSize=128
iconSubFolder=${iconSize}x${iconSize} # could also be: scalable or 256x256 or 512x512

# reference: https://download-origin.cdn.mozilla.net/pub/thunderbird/releases/latest/README.txt
urlLatest="https://download.mozilla.org/?product=thunderbird-beta-latest-SSL&os=linux64&lang=es-ES"

function printUsage () {
  echo
  echo "-------------------------------------------------------"
  echo "This script \"$(basename $0)\" will download and install ${appname_proper} from Mozilla."
  echo "Run with sudo for system-wide installation."
  echo "Parameters:"
  echo " -q : optional, exclusive, will only detect if an update is available"
  echo " -s : optional, exclusive, will only restore symlink(s)"
  echo " -x : optional, exclusive, will uninstall the app"
  echo " -y : optional, exclusive, will auto-upgrade when a new version is found"
  echo
  echo "When no params passed, will ask for confirmation before installation."
  echo
  echo "Examples:"
  echo "# $(basename $0)"
  echo "# $(basename $0) -q"
  echo "# $(basename $0) -s"
  echo "# $(basename $0) -x"
  echo "# $(basename $0) -y"
  echo "-------------------------------------------------------"
  echo
}

function pause () {
  read -p "$*"
}

function haveConnection() {

  wget -q --spider https://dns.google/ >/dev/null
  if [ $? -eq 0 ]; then
    echo "true"
  else
    echo "false"
  fi
}

function setSymlink () {
  # param 1 is "what"
  # param 2 is "where"
  # as in: ln -s what where...

  echo -n "Symlinking \"$1\" to \"$2\"..."
  rm -f "$2" &> /dev/null
  #pause "after removing $2..."
  ln -s "$1" "$2" &> /dev/null
  echo " done"
  #pause "after restoring $2 to $1..."
}

function getTrashMsg () {
  local trashMsg=
  if [[ "${deleteToTrash}" == "true" ]]; then
    trashMsg=' to trash'
  fi
  echo "${trashMsg}"
}

function deleteFile () {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    # the file or symlink exists
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -f "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function deleteFolder () {
  if [[ -d "$1" ]]; then
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -rf "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function cleanup () {
  echo "Cleanup..."
  deleteFolder "${tempdlfolder}"
  cd "${initialDir}"
  echo "Cleanup done."
}

function getEffectiveUrl () {
  local initialUrl=$1
  # see where the url is forwarded:
  local fwdurl=$(curl -o /dev/null -w "%{url_effective}" -I -L -s -S ${initialUrl})
  echo "${fwdurl}"
}

function handleExitStatus () {
  # arg1: status code; arg2: message
  local EXIT_STATUS=$1
  if [ ${EXIT_STATUS} -ne 0 ]; then
    # failure
    local ERR_MSG=$2
    if [[ -z "${ERR_MSG}" ]]; then
      ERR_MSG="${appname_proper} installation failed."
    fi
    echo
    echo "${ERR_MSG}"
    cleanup
    echo
    exit ${exitStatusFail}
  fi
}

function downloadFile () {
  # param 1: url
  # param 2: file path

  local locUrl="$1"
  local locFilePath="$2"
  deleteFile "${locFilePath}"
  echo "Downloading file: ${locFilePath}..."
  # see where the download is forwarded (cdn):
  local fwdurl=$(getEffectiveUrl "${locUrl}")
  echo "From: ${fwdurl}..."

  wget -qnv --show-progress -O "${locFilePath}" "${fwdurl}"
  if [[ ! -f "${locFilePath}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Failed to download file: \"${locFilePath}\" from \"${locUrl}\""
  else
    echo " done."
  fi
}

function removeAlternatives () {
  #update-alternatives --remove-all gnome-www-browser
  #update-alternatives --remove-all x-www-browser
  #update-alternatives --remove-all www-browser
  update-alternatives --remove-all ${appname}
}

function restoreAlternatives () {
  removeAlternatives

  # usage:
  # update-alternatives --install symlink name path priority
  # we can only have one alternative per 'symlink' and 'name':
  update-alternatives --install "$2" ${appname} "$1" 1000
  #update-alternatives --install /usr/bin/x-www-browser x-www-browser "$1" 900
  #update-alternatives --install /usr/bin/www-browser www-browser "$1" 800
  #update-alternatives --install /usr/bin/gnome-www-browser gnome-www-browser "$1" 700
}

function restoreSymlinks () {
  setSymlink "$1" "$2"
  local altFolder=/etc/alternatives
  if [[ -d "${altFolder}" ]]; then
    restoreAlternatives "$1" "$2"
  fi
}

function prepare () {

  initialDir=$(pwd)
  tempdlfolder=/tmp/##ff_temp_dl##
  bundle=##${appname}_unknown_filename##

  systemWide=false
  shortcutDir=${HOME}/.local/bin
  installPath=${HOME}/.local/lib
  installDesktopDir=${HOME}/.local/share/applications
  installIconDir=${HOME}/.local/share/icons/hicolor/${iconSubFolder}/apps

  local installVerb=install
  if [[ "${doUninstall}" == "true" ]]; then
    installVerb=uninstall
  elif [[ "${optQueryUpdate}" == "true" ]]; then
    installVerb='query version'
  fi

  # check for system-wide installation
  if [ "$(id -u)" == "0" ]; then
    systemWide=true
    shortcutDir=/usr/local/bin
    installPath=/opt
    installDesktopDir=/usr/share/applications
    installIconDir=/usr/share/icons/hicolor/${iconSubFolder}/apps
    echo "Will ${installVerb} system-wide."
  else
    echo "Will ${installVerb} for current user only."
  fi

  appInstallDir=${installPath}/${appname}
  shortcutFile=${shortcutDir}/${appname}
  desktopFile=${installDesktopDir}/${appname}.desktop
  iconFile=${installIconDir}/${iconName}
  appInstallBinary=${installPath}/${appname}/${appname}
}

function checkInstallPath () {
  echo -n "Checking if installation path exists: ${installPath}..."
  if [[ ! -d "${installPath}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: installation path not found: ${installPath}."
  else
    echo " it does."
  fi
}

function detectAppVersions () {

  if [[ "${doUninstall}" != "true" ]]; then
    # find where the URL is forwarded (302):
    local fwdurl=$(getEffectiveUrl ${urlLatest})
    remotever=$(echo ${fwdurl} | sed -E 's|.*releases/([^/]+)/.*|\1|gI')
    bundle=$(echo ${fwdurl} | sed -E 's|.*/([^/]+)$|\1|gI')
    localver=NONE

    # instead of: `command -v`, `which` could also work...
    if ! [[ -x "$(command -v ${appname})" ]] ; then
      echo "${appname_proper} is not currently installed"
    else
      if [[ "${systemWide}" == "true" ]]; then
        # root cannot run thunderbird command in terminal, so we need to impersonate a normal user:
        local normalUser=$(awk -v val=1000 -F ":" '$3==val{print $1}' /etc/passwd)
        localver=$(runuser -l ${normalUser} -c "${appname} --version" | sed -E 's/[^\.0-9]+([0-9]+[\.0-9]*)/\1/gI')
      else
        localver=$(${appname} --version | sed -E 's/[^\.0-9]+([0-9]+[\.0-9]*)/\1/gI')
      fi
    fi

    echo "Remote ${appname_proper} version found: ${remotever}."
    echo "Local  ${appname_proper} version found: ${localver}."
  fi
}

function checkArguments () {

  if [ ${numargs} -gt 0 ]; then

    echo "Argument(s) passed: ${allargs[@]}."

    # uninstall is exclusive
    if [[ "${doUninstall}" == "true" ]]; then
      optRestoreSymlinks=false
      optAutoUpgrade=false
    fi

    # if user requested to restore symlinks, it must be exclusive!
    if [[ "${optRestoreSymlinks}" == "true" ]]; then
      optAutoUpgrade=false

      # act on "restore symlinks" option:
      restoreShortcuts
      exit ${exitStatusSuccess}
    fi
  fi

  if [[ "${doUninstall}" != "true" ]]; then
    checkArchitecture
  fi
}

function checkArchitecture () {

  # architecture can be read this way (returns 64 for 64-bit):
  # or this way (returns x86_64 for 64-bit):
  # arch=$(uname -m)
  local arch=$(getconf LONG_BIT)

  if [ ${arch} -ne 64 ]; then
    handleExitStatus ${exitStatusFail} "Error: CPU architecture mismatch (not 64-bit), aborting installation."
  fi
}

function decisionBlock () {

  # if no args, let user decide
  if [ ${numargs} -lt 1 ]; then
    read -p "Install latest version available ${remotever}? [y/N] : " decision

    if [[ ${decision} != [yY]* ]]; then
      handleExitStatus ${exitStatusFail} "Error: User denied installation. Aborting."
    fi
  else
    # some args were passed
    if [[ "${doUninstall}" == "true" ]]; then

      read -p "Uninstall ${appname}? [y/N] : " decision

      if [[ ${decision} != [yY]* ]]; then
        handleExitStatus ${exitStatusFail} "Error: User declined un-installation. Aborting."
      fi
    elif [[ "${optQueryUpdate}" == "true" ]]; then
      cleanup
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "${appname_proper} latest version ${remotever} already installed."
        exit ${exitStatusSuccess}
      fi
      echo "${appname_proper} update available: ${remotever}"
      exit ${exitStatusUpdateAvailable}
    else
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "Latest ${appname_proper} version ${remotever} already installed, nothing to do."
        cleanup
        exit ${exitStatusSuccess}
      fi

      if [[ "${optAutoUpgrade}" == "true" ]]; then
        echo "Will upgrade to version: ${remotever}..."
      fi

      checkInstallPath
    fi
  fi

  if [[ "${doUninstall}" == "true" ]]; then
    uninstall
  else
    performInstall
  fi
}

function downloadBundle () {

  bundlePath=${tempdlfolder}/${bundle}

  downloadFile "${urlLatest}" "${bundlePath}"

  local filesize=$(stat -c %s "${bundlePath}")
  if [ ${filesize} -lt 1024000 ]; then
    handleExitStatus ${exitStatusFail} "Error: Downloaded bundle file size (${filesize} bytes) too small for: ${bundlePath}."
  fi
}

function verifyChecksum () {
  # release folder:
  # https://releases.mozilla.org/pub/thunderbird/releases/118.0
  local baseRls=https://releases.mozilla.org/pub/thunderbird/releases/${remotever}

  # dload 3 files: gpg pub key, a signature file and a checksum file.
  local keyurl=${baseRls}/KEY
  local keyfile=${tempdlfolder}/mozilla-pub.key
  downloadFile "${keyurl}" "${keyfile}"

  gpg --import "${keyfile}"
  handleExitStatus $? "Error: Failed to import Mozilla GPG key."

  local sigfile=SHA512SUMS.asc
  local sigurl=${baseRls}/${sigfile}
  local sigfilepath=${tempdlfolder}/${sigfile}
  downloadFile "${sigurl}" "${sigfilepath}"

  local shafile=SHA512SUMS
  local shaurl=${baseRls}/${shafile}
  local shafilepath=${tempdlfolder}/${shafile}
  downloadFile "${shaurl}" "${shafilepath}"

  pushd "${tempdlfolder}" &>/dev/null
  gpg --verify ${sigfile} ${shafile}
  handleExitStatus $? "Error: Failed to verify authenticity of checksum files."
  popd &>/dev/null

  local shalinesrch=linux-x86_64/es-ES/${bundle}
  echo -n "Verifying SHA512 checksum..."

  local grepline=$(grep "${shalinesrch}" "${shafilepath}" | head -1)
  local checkline=$(echo "${grepline}" | awk '{print $1}')'  '"${bundle}"

  pushd "${tempdlfolder}" &>/dev/null
  echo "${checkline}" | sha512sum -c --strict --status
  popd &>/dev/null

  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    echo " Pass."
  else
    echo " Failed!"
    handleExitStatus ${exitStatusFail}
  fi

  # read -p "Stopped for debug. Continue? : [Y/n] : " decision
  # if [[ ${decision} == [nN]* ]]; then
  #   echo "Script execution stopped."
  #   cleanup
  #   exit 1
  # fi
}

function deployApp () {
  # we will be extracting the archive in a folder we want:
  echo "Cleanup destination folder before install..."
  deleteFile "${shortcutFile}"
  deleteFolder "${appInstallDir}"
  echo "...done."

  mkdir -p "${appInstallDir}"

  handleExitStatus $? "Failed to create installation folder: ${appInstallDir}."

  cd "${appInstallDir}"
  echo -n "Extracting archive..."
  tar -xvjf "${bundlePath}" -C "${appInstallDir}" --strip-components=1 &> /dev/null

  handleExitStatus $? "Failed to extract bundle to: ${appInstallDir}."
  echo " done."

  echo -n "Waiting 3 seconds..."
  sleep 3
  echo " done."
}

function restoreShortcuts () {
  echo "Restoring alternatives and symlinks..."
  restoreSymlinks "${appInstallBinary}" "${shortcutFile}"
  echo "Alternatives and symlinks restored."
}

function deployIcons () {
  deleteFile "${iconFile}"

  cp "${appInstallDir}/thunderbird/chrome/icons/default/default128.png" "${iconFile}"
}

function deployDesktopFile () {
  deleteFile "${desktopFile}"

  echo -n "Creating desktop file: ${desktopFile}..."

  echo "[Desktop Entry]" > ${desktopFile}
  handleExitStatus $? "Failed to create desktop file: ${desktopFile}."

  echo "Version=1.0" >> ${desktopFile}
  echo "Encoding=UTF-8" >> ${desktopFile}
  echo "Name=${appname_proper}" >> ${desktopFile}
  echo "Comment=Send and receive mail with Thunderbird" >> ${desktopFile}
  echo "GenericName=Mail App" >> ${desktopFile}
  echo "X-GNOME-FullName=Mozilla ${appname_proper} Mail App" >> ${desktopFile}
  echo "Exec=${appname} %u" >> ${desktopFile}
  echo "Terminal=false" >> ${desktopFile}
  echo "Type=Application" >> ${desktopFile}
  echo "Icon=${appname}" >> ${desktopFile}
  echo "X-AppImage-Version=${remotever}" >> ${desktopFile}
  echo "Categories=Application;Network;Email;" >> ${desktopFile}
  echo "MimeType=x-scheme-handler/mailto;application/x-xpinstall;" >> ${desktopFile}

  echo " done."
}

function uninstall () {
  echo "Uninstalling ${appname}..."
  deleteFolder "${appInstallDir}"
  deleteFile "${shortcutFile}"
  deleteFile "${desktopFile}"
  deleteFile "${iconFile}"
  removeAlternatives
  echo "...uninstalled successfully."
}

function performInstall () {

  echo "Installing latest ${appname_proper}..."

  # final temp working folder:
  tempdlfolder=$(mktemp -d)

  downloadBundle
  verifyChecksum
  deployApp
  restoreShortcuts
  deployIcons
  deployDesktopFile
}

function main () {

  # read -p "Press any key to continue: " decision
  local haveInternet=$(haveConnection)
  if [ "${haveInternet}" != "true" ]; then
    handleExitStatus ${exitStatusFail} "Internet is unreachable"
  fi

  prepare

  checkArguments

  detectAppVersions

  decisionBlock

  cleanup

  local msg="${appname_proper} installation completed!"
  if [[ "${doUninstall}" != "true" ]]; then
    msg="${msg} Run as normal user: >\$ ${appname} --version"
  fi
  echo
  echo "${msg}"
  echo
}

############
### main ###
############

echo "> \"$(basename $0)\" will download and install ${appname_proper} from Mozilla."
# checking arguments cannot be done in a function
# list of arguments expected in the input
optstring=":qsxy"
while getopts ${optstring} arg; do
  case $arg in
    q)
      optQueryUpdate=true
      echo " Will query for an available update."
      ;;
    s)
      optRestoreSymlinks=true
      echo " Will only restore symlinks."
      ;;
    x)
      doUninstall=true
      echo "Will uninstall ${appname}."
      ;;
    y)
      optAutoUpgrade=true
      echo " Will upgrade if any update is available."
      ;;
    *)
      echo " Unknown argument(s): -${arg}. Exiting."
      printUsage
      exit ${exitStatusFail}
      ;;
  esac
done

main

exit ${exitStatusSuccess}
