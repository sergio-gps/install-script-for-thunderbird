#!/bin/bash
#
# This script will automagically download and install the VSCodium appimage
# from: https://github.com/VSCodium/vscodium/releases/latest
#
# Run with sudo to install system-wide, or without to install locally.
# 1. run without any parameters to download and install with confirmation.
# 2. pass parameter: -q in order to query whether an update is available.
# 3. pass parameter: -x to uninstall the app
# 4. pass parameter: -y in order to auto-upgrade when a new version is found.
#
# Depends on: jq, sed, awk, tar, sleep, egrep, getconf, curl, wget, sha256sum, test, tr, trash-cli
#

appname=codium
appname_proper=VSCodium
#appname_proper=$(echo ${appname} | sed -E 's/^(.)(.*)$/\u\1\2/')
appname_proper_lc=$(echo ${appname_proper} | sed -E 's|^(.*)$|\L\1|')
iconName=${appname_proper_lc}.png
# iconName=$(echo ${appname_proper}.png | tr '[:upper:]' '[:lower:]')

allargs=("$@")
numargs=$#

doUninstall=false
optQueryUpdate=false
optAutoUpgrade=false
exitStatusSuccess=0
exitStatusFail=1
exitStatusUpdateAvailable=100
deleteToTrash=false

systemWide=
shortcutDir=
installPath=/opt
appInstallDir=
tempdlfolder=/tmp/##${appname}_temp_dl##
bundle=##${appname}_unknown_filename##

urlLatest=https://api.github.com/repos/VSCodium/vscodium/releases/latest
iconSize=1024
iconSubFolder=${iconSize}x${iconSize} # could also be: scalable or 256x256 or 512x512

function printUsage () {
  echo
  echo "-------------------------------------------------------"
  echo "This script \"$(basename $0)\" will download and install ${appname_proper} from github."
  echo "Run with sudo to install system-wide, or without to install locally."
  echo "Parameters:"
  echo " -q : optional, exclusive, will only detect if an update is available"
  echo " -x : optional, exclusive, will uninstall the app"
  echo " -y : optional, exclusive, will auto-upgrade when a new version is found"
  echo
  echo "When no params passed, will ask for confirmation before installation."
  echo
  echo "Examples:"
  echo "\$ $(basename $0)"
  echo "\$ $(basename $0) -q"
  echo "\$ $(basename $0) -x"
  echo "\$ $(basename $0) -y"
  echo "-------------------------------------------------------"
  echo
}

function pause () {
  read -p "$*"
}

function fileExists () {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    echo "true"
  else
    echo "false"
  fi
}

function setSymlink () {
  # param 1 is "what"
  # param 2 is "where"
  # as in: ln -s what where...

  echo -n "Symlinking \"$1\" to \"$2\"..."
  rm -f "$2" &> /dev/null
  #pause "after removing $2..."
  ln -s "$1" "$2" &> /dev/null
  echo " done"
  #pause "after restoring $2 to $1..."
}

function getTrashMsg () {
  local trashMsg=
  if [[ "${deleteToTrash}" == "true" ]]; then
    trashMsg=' to trash'
  fi
  echo "${trashMsg}"
}

function deleteFile () {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    # the file or symlink exists
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -f "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function deleteFolder () {
  if [[ -d "$1" ]]; then
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -rf "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function cleanup () {
  echo "Cleanup..."
  deleteFolder "${tempdlfolder}"
  if [[ -d "${initialDir}" ]]; then
    cd "${initialDir}"
  fi
  echo "Cleanup done."
}

function getEffectiveUrl () {
  local initialUrl=$1
  # see where the url is forwarded:
  local fwdurl=$(curl -o /dev/null -w "%{url_effective}\n" -I -L -s -S ${initialUrl})
  echo "${fwdurl}"
}

function handleExitStatus () {
  # arg1: status code; arg2: message
  local EXIT_STATUS=$1
  if [ ${EXIT_STATUS} -ne 0 ]; then
    # failure
    local ERR_MSG=$2
    if [[ -z "${ERR_MSG}" ]]; then
      ERR_MSG="${appname_proper} installation failed."
    fi
    echo
    echo "${ERR_MSG}"
    cleanup
    echo
    exit ${exitStatusFail}
  fi
}

function downloadFile () {
  # param 1: url
  # param 2: file path (destination)

  local locUrl="$1"
  local locFilePath="$2"
  deleteFile "${locFilePath}"
  echo "Downloading file: ${locFilePath}..."
  # see where the download is forwarded (cdn):
  local fwdurl=$(getEffectiveUrl "${locUrl}")
  echo "From: ${fwdurl}..."

  wget -qnv --show-progress -O "${locFilePath}" "${fwdurl}"
  if [[ ! -f "${locFilePath}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Failed to download file: \"${locFilePath}\" from \"${locUrl}\""
  else
    echo " done."
  fi
}

function prepare () {

  systemWide=false
  initialDir=$(pwd)

  shortcutDir=${HOME}/.local/bin
  installPath=${HOME}/.local/lib
  installDesktopDir=${HOME}/.local/share/applications
  installIconDir=${HOME}/.local/share/icons/hicolor/${iconSubFolder}/apps

  local installVerb=install
  if [[ "${doUninstall}" == "true" ]]; then
    installVerb=uninstall
  elif [[ "${optQueryUpdate}" == "true" ]]; then
    installVerb='query version'
  fi

  # check for system-wide installation
  if [ "$(id -u)" == "0" ]; then
    systemWide=true
    deleteToTrash=false
    shortcutDir=/usr/local/bin
    installPath=/opt
    installDesktopDir=/usr/share/applications
    installIconDir=/usr/share/icons/hicolor/${iconSubFolder}/apps
    echo "Will ${installVerb} system-wide."
  else
    echo "Will ${installVerb} for current user only."
  fi

  appInstallDir=${installPath}/${appname}
  shortcutFile=${shortcutDir}/${appname}
  desktopFile=${installDesktopDir}/${appname}.desktop
  iconFile=${installIconDir}/${iconName}
}

function checkInstallPath () {
  echo -n "Checking if installation path exists: ${installPath}..."
  if [[ ! -d "${installPath}" ]]; then
    echo
    echo "Creating directory: ${installPath}."
    mkdir -p "${installPath}"
    handleExitStatus $? "Error: cannot create installation path: ${installPath}."
  else
    echo " it does."
  fi
}

function detectAppVersions () {

  if [[ "${doUninstall}" != "true" ]]; then

    echo "Detecting latest release version from: ${urlLatest} ..."

    local fwdurl=$(getEffectiveUrl ${urlLatest})
    latestJson=$(curl -s -S "${fwdurl}" | jq .)

    # look for: { tag_name: 1.76.2.23074 }
    remotever=$(echo "${latestJson}" | jq -r .tag_name)
    localver=UNKNOWN

    # instead of: `command -v`, `which` could also work...
    if ! [[ -x "$(command -v ${appname})" ]] ; then
      echo "${appname_proper} is not currently installed"
    else
      # find the installed desktop file and extract the version from it
      local dtopExists=$(fileExists "${desktopFile}")
      if [[ "${dtopExists}" == "true" ]]; then
        # parse the line: X-AppImage-Version=1.76.2.23074.glibc2.17
        # get only the version numbers before 'glibc'
        localver=$(egrep -i 'X-AppImage-Version=' "${desktopFile}" | sed -E 's|^.+=([0-9\.]+?)(\.?[a-zA-Z]+?).*$|\1|gI')
      else
        echo "Cannot detect local version of ${appname_proper} (desktop file not found)"
      fi
    fi

    echo "Remote ${appname_proper} version found: ${remotever}."
    echo "Local  ${appname_proper} version found: ${localver}."
  fi
}

function checkArguments () {

  if [ ${numargs} -gt 0 ]; then
    echo "Argument(s) passed: ${allargs[@]}."

    # uninstall is exclusive
    if [[ "${doUninstall}" == "true" ]]; then
      optAutoUpgrade=false
    fi
  fi
}

function decisionBlock () {

  # if no args, let user decide
  if [ ${numargs} -lt 1 ]; then
    read -p "Install latest version available ${remotever}? [y/N] : " decision

    if [[ ${decision} != [yY]* ]]; then
      handleExitStatus ${exitStatusFail} "User denied installation. Aborting."
    fi
  else
    # some args were passed
    if [[ "${doUninstall}" == "true" ]]; then

      read -p "Uninstall ${appname}? [y/N] : " decision

      if [[ ${decision} != [yY]* ]]; then
        handleExitStatus ${exitStatusFail} "User declined un-installation. Aborting."
      fi
    elif [[ "${optQueryUpdate}" == "true" ]]; then
      cleanup
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "${appname_proper} latest version ${remotever} already installed."
        exit ${exitStatusSuccess}
      fi
      echo "${appname_proper} update available: ${remotever}"
      exit ${exitStatusUpdateAvailable}
    else
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "Latest ${appname_proper} version ${remotever} already installed, nothing to do."
        cleanup
        exit ${exitStatusSuccess}
      fi

      if [[ "${optAutoUpgrade}" == "true" ]]; then
        echo "Will upgrade to version: ${remotever}..."
      fi

      checkInstallPath
    fi
  fi

  if [[ "${doUninstall}" == "true" ]]; then
    uninstall
  else
    performInstall
  fi
}

function downloadBundle () {

  local appBinaryJson=$(echo "${latestJson}" | jq -r '.assets[] | select(.name|test("^'${appname_proper_lc}'[-]linux[-]x64[-].+[.]tar[.]gz$"; "ins"))')

  if [[ -z "${appBinaryJson}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Cannot find tar asset in release list."
  fi

  bundle=$(echo ${appBinaryJson} | jq -r '.name')
  if [[ -z "${bundle}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Cannot find tar bundle."
  fi

  tempdlfolder=$(mktemp -d)
  bundlePath="${tempdlfolder}/${bundle}"

  local urlDownloadBundle=$(echo ${appBinaryJson} | jq -r '.browser_download_url')
  if [[ -z "${urlDownloadBundle}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Cannot find tar bundle download URL."
  fi
  downloadFile "${urlDownloadBundle}" "${bundlePath}"
}

function verifyChecksum () {
  # download sha256sums file
  local testexpr=${bundle}.sha256\$
  local checksumJson=$(echo ${latestJson} | jq -r --arg TEXPR "$testexpr" '.assets[] | select(.name|test($TEXPR; "ins"))')
  local checksumFile=$(echo ${checksumJson} | jq -r '.name')
  local checksumURL=$(echo ${checksumJson} | jq -r '.browser_download_url')
  local checksumPath="${tempdlfolder}/${checksumFile}"

  downloadFile "${checksumURL}" "${checksumPath}"

  cd "${tempdlfolder}"
  echo -n "Verifying SHA256 checksum..."
  # logic kept while converting it from original:
  # grep node-v18.15.0-linux-x64.tar.xz SHASUMS256.txt | sha256sum -c --strict -
  local grepline=$(grep "${bundle}" "${checksumFile}" | head -1)
  echo "${grepline}" | sha256sum -c --strict --status

  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    echo " Pass."
  else
    echo " Failed!"
    cleanup
    exit ${exitStatusFail}
  fi
}

function deployApp () {

  echo "Cleanup destination folder before install..."
  deleteFile "${shortcutFile}"
  deleteFile "${iconFile}"
  deleteFolder "${appInstallDir}"
  echo "...done."

  # read -p "Stopped for debug. Continue? : [Y/n] : " decision
  # if [[ ${decision} == [nN]* ]]; then
  #     echo "Script execution stopped."
  #     exit 1
  # fi

  mkdir -p "${appInstallDir}"
  handleExitStatus $? "Error: Failed to create installation folder: ${appInstallDir}."

  # extract tar.gz
  echo -n "Extracting archive..."
  tar -xf "${bundlePath}" --directory "${appInstallDir}" &> /dev/null
  handleExitStatus $? "Error: Failed to deploy application."
  echo " done."
  echo "Application deployed."

  chmod +x "${appInstallDir}/${appname}"
}

function restoreShortcuts () {
  setSymlink "${appInstallDir}/${appname}" "${shortcutFile}"
}

function deployIcons () {
  deleteFile "${iconFile}"

  local iconPathOrigin="${appInstallDir}/resources/app/resources/linux/code.png"
  local iconPathOriginExists=$(fileExists "${iconPathOrigin}")
  if [[ "${iconPathOriginExists}" != "true" ]]; then
    echo "Warning: could not find icon in extracted tarball."
  else
    # copy it into final location
    echo -n "Deploying icon file: ${iconName} into ${installIconDir}/..."
    mkdir -p "${installIconDir}" &> /dev/null
    cp "${iconPathOrigin}" "${iconFile}" &> /dev/null
    if [[ ! -f "${iconFile}" ]]; then
      echo
      echo "Warning: Failed to copy icon file: ${iconFile}."
    else
      echo " done."
    fi
  fi

  if [[ "${systemWide}" == "true" ]]; then
    # also copy the icon to pixmaps folder
    mkdir -p /usr/share/pixmaps/ &> /dev/null
    cp "${iconPathOrigin}" /usr/share/pixmaps/ &> /dev/null
  fi
}

function deployDesktopFile () {

  deleteFile "${desktopFile}"

  echo -n "Deploying desktop file: ${desktopFile}..."
  touch "${desktopFile}" &> /dev/null
  handleExitStatus $? "Error: Failed to create desktop file \"${desktopFile}\""

  echo '[Desktop Entry]' > "${desktopFile}"
  echo 'Version=1.0' >> "${desktopFile}"
  echo 'Encoding=UTF-8' >> "${desktopFile}"
  echo "Name=${appname_proper}" >> "${desktopFile}"
  echo 'Comment=Code Editing. Redefined.' >> "${desktopFile}"
  echo 'GenericName=Text Editor' >> "${desktopFile}"
  echo "Exec=${appInstallDir}/${appname} --unity-launch %F" >> "${desktopFile}"
  echo "Icon=${appname_proper_lc}" >> "${desktopFile}"
  echo 'Type=Application' >> "${desktopFile}"
  echo 'StartupNotify=false' >> "${desktopFile}"
  # echo 'StartupWMClass=VSCodium' >> "${desktopFile}"
  echo 'Categories=TextEditor;Development;IDE;' >> "${desktopFile}"
  echo 'MimeType=text/plain;inode/directory;application/x-codium-workspace;' >> "${desktopFile}"
  echo 'Actions=new-empty-window;' >> "${desktopFile}"
  echo 'Keywords=code;vscode;codium;vscodium;' >> "${desktopFile}"
  echo "X-AppImage-Version=${remotever}" >> "${desktopFile}"
  echo >> "${desktopFile}"
  echo '[Desktop Action new-empty-window]' >> "${desktopFile}"
  echo 'Name=New Empty Window' >> "${desktopFile}"
  echo "Exec=${appInstallDir}/${appname} --new-window %F" >> "${desktopFile}"
  echo "Icon=${appname_proper_lc}" >> "${desktopFile}"

  echo " done."
}

function uninstall () {
  echo "Uninstalling ${appname}..."
  deleteFolder "${appInstallDir}"
  deleteFile "${shortcutFile}"
  deleteFile "${desktopFile}"
  deleteFile "${iconFile}"
  echo "...uninstalled successfully."
}

function performInstall () {

  echo "Installing latest ${appname_proper}..."

  downloadBundle
  verifyChecksum

  deployApp
  restoreShortcuts
  deployIcons
  deployDesktopFile
}

function main () {

  # read -p "Press any key to continue: " decision
  prepare

  checkArguments

  detectAppVersions

  decisionBlock

  cleanup

  echo
  echo "${appname_proper} installation completed!"
  echo

}

############
### main ###
############

echo "> \"$(basename $0)\" will download and install ${appname_proper} from github."
# checking arguments cannot be done in a function
# list of arguments expected in the input
optstring=":qxy"
while getopts ${optstring} arg; do
  case $arg in
    q)
      optQueryUpdate=true
      echo " Will query for an available update."
      ;;
    x)
      doUninstall=true
      echo "Will uninstall ${appname}."
      ;;
    y)
      optAutoUpgrade=true
      echo "Will upgrade if any update is available."
      ;;
    *)
      echo "Unknown argument(s): -${arg}. Exiting."
      printUsage
      exit ${exitStatusFail}
      ;;
  esac
done

main

exit ${exitStatusSuccess}
