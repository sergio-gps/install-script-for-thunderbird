#!/bin/bash
#
# This script will download and install locally the GitAhead git GUI program/app
# Home page: https://gitahead.github.io/gitahead.com/
# Releases page: https://github.com/gitahead/gitahead/releases
#
# Run with sudo to install system-wide, or without to install locally.
# 1. run without any parameters to download and install with confirmation.
# 2. pass parameter: -y in order to auto-upgrade when a new version is found.
# 3. pass parameter: -x to uninstall the app
#
# NOTE: the provided archive does a messy install -- so:
# This script depends on 7z utility -- the only one that can properly untar the provided binary
#
# Depends on: 7z,  sed, awk, runuser, tar, sleep, egrep, getconf, curl, wget, sha256sum, test, trash-cli
#

appname=gitahead
appname_proper=GitAhead

allargs=("$@")
numargs=$#

doUninstall=false
optAutoUpgrade=false
exitStatusSuccess=0
exitStatusFail=1
deleteToTrash=false

function printUsage () {
  echo
  echo "-------------------------------------------------------"
  echo "This script \"$(basename $0)\" will download and install ${appname_proper} from github."
  echo "Run with sudo to install system-wide, or without to install locally."
  echo "Parameters:"
  echo " -y : optional, exclusive, will auto-upgrade when a new version is found"
  echo " -x : optional, exclusive, will uninstall the app"
  echo
  echo "When no params passed, will ask for confirmation before installation."
  echo
  echo "Examples:"
  echo "\$ $(basename $0)"
  echo "\$ $(basename $0) -y"
  echo "\$ $(basename $0) -x"
  echo "-------------------------------------------------------"
  echo
}

# checking arguments cannot be done in a function
# list of arguments expected in the input
optstring=":yx"
while getopts ${optstring} arg; do
  case $arg in
    x)
      doUninstall=true
      echo "Will uninstall ${appname}."
      ;;
    y)
      optAutoUpgrade=true
      echo "Will upgrade if any update is available."
      ;;
    *)
      echo "Unknown argument(s): -${arg}. Exiting."
      printUsage
      exit ${exitStatusFail}
      ;;
  esac
done

function pause () {
  read -p "$*"
}

function setSymlink () {
  # param 1 is "what"
  # param 2 is "where"
  # as in: ln -s what where...

  echo -n "Symlinking \"$1\" to \"$2\"..."
  rm -f "$2" &> /dev/null
  #pause "after removing $2..."
  ln -s "$1" "$2" &> /dev/null
  echo " done"
  #pause "after restoring $2 to $1..."
}

function getTrashMsg () {
  local trashMsg=
  if [[ "${deleteToTrash}" == "true" ]]; then
    trashMsg=' to trash'
  fi
  echo "${trashMsg}"
}

function deleteFile () {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    # the file or symlink exists
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -f "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function deleteFolder () {
  if [[ -d "$1" ]]; then
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -rf "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function cleanup () {
  echo "Cleanup..."
  deleteFolder "${tempdlfolder}"
  cd "${initialDir}"
  echo "Cleanup done."
}

function getEffectiveUrl () {
  local initialUrl=$1
  # see where the url is forwarded:
  local fwdurl=$(curl -o /dev/null -w "%{url_effective}\n" -I -L -s -S ${initialUrl})
  echo "${fwdurl}"
}

function prepare () {
  systemWide=false
  tempdlfolder=/tmp/##${appname}_temp_dl##
  initialDir=${PWD}
  urlLatest=https://api.github.com/repos/gitahead/gitahead/releases/latest
  shortcutDir=${HOME}/.local/bin
  installPath=${HOME}/.local/lib
  installDesktopDir=${HOME}/.local/share/applications
  installIconDir=${HOME}/.local/share/icons/hicolor/512x512/apps

  local installVerb=install
  if [[ "${doUninstall}" == "true" ]]; then
    installVerb=uninstall
  fi

  # check for system-wide installation
  if [ "$(id -u)" == "0" ]; then
    systemWide=true
    shortcutDir=/usr/local/bin
    installPath=/opt
    installDesktopDir=/usr/share/applications
    installIconDir=/usr/share/icons/hicolor/512x512/apps
    echo "Will ${installVerb} system-wide."
  else
    echo "Will ${installVerb} for current user only."
  fi

  appInstallDir=${installPath}/${appname}
  shortcutFile=${shortcutDir}/${appname}
  desktopFile=${installDesktopDir}/${appname}.desktop
}

function checkInstallPath () {
  echo -n "Checking if installation path exists: ${installPath}..."
  if [[ ! -d "${installPath}" ]]; then
    echo
    echo "Creating directory: ${installPath}."
    mkdir -p "${installPath}"

    local EXIT_STATUS=$?
    if [ ${EXIT_STATUS} -ne 0 ]; then
      # failure
      echo "Failed to create installation folder: ${installPath}."
      cleanup
      exit ${exitStatusFail}
    fi

  else
    echo " it does."
  fi
}

function detectAppVersions () {

  echo "Detecting latest release version from: ${urlLatest} ..."

  local fwdurl=$(getEffectiveUrl ${urlLatest})
  latestJson=$(curl -s -S "${fwdurl}" | jq . )

  # look for: { tag_name: v2.6.3 }
  remotever=$(echo "${latestJson}" | jq -r .tag_name | sed -E 's|.*v([\.0-9]+).*|\1|gI')
  localver=UNKNOWN

  # instead of: `command -v`, `which` could also work...
  if ! [[ -x "$(command -v ${appname})" ]] ; then
    echo "${appname_proper} is not currently installed"
  else
    localver=$(${appname} --version | sed -E 's|.*\s+([\.0-9]+).*|\1|gI')
  fi

  echo "Remote ${appname_proper} version found: ${remotever}."
  echo "Local  ${appname_proper} version found: ${localver}."
}

function checkArguments () {

  if [ ${numargs} -gt 0 ]; then
    echo "Argument(s) passed: ${allargs[@]}."

    # uninstall is exclusive
    if [[ "${doUninstall}" == "true" ]]; then
      optAutoUpgrade=false
    fi
  fi

  if [[ "${doUninstall}" != "true" ]]; then
    detectAppVersions
  fi
}

function decisionBlock () {

  # if no args, let user decide
  if [ ${numargs} -lt 1 ]; then
    read -p "Install latest version available ${remotever}? [y/N] : " decision

    if [[ ${decision} != [yY]* ]]; then
      echo "Error: User denied installation. Aborting."
      cleanup
      exit ${exitStatusFail}
    fi
  else
    # some args were passed
    if [[ "${doUninstall}" == "true" ]]; then

      read -p "Uninstall ${appname}? [y/N] : " decision

      if [[ ${decision} != [yY]* ]]; then
        echo "Error: User declined un-installation. Aborting."
        cleanup
        exit ${exitStatusFail}
      fi

    else
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "Latest version ${remotever} already installed, nothing to do."
        cleanup
        exit ${exitStatusSuccess}
      fi

      if [[ "${optAutoUpgrade}" == "true" ]]; then
        echo "Will upgrade to version: ${remotever}..."
      fi

      checkInstallPath
    fi
  fi

  if [[ "${doUninstall}" == "true" ]]; then
    uninstall
  else
    performInstall
  fi
}

function downloadBundle () {

  # the following expression doesn't work: test("^gitahead.+[.]sh$","ins")
  # because the docs at https://stedolan.github.io/jq/manual/#RegularexpressionsPCRE
  # are wrong, even if we specified 'i' flag for case insensitivity.
  # find the asset that has a name like: GitAhead-2.6.3.sh
  local appImageJson=$(echo ${latestJson} | jq -r '.assets[] | select(.name|test("^.+[.]sh$"; "ins"))')
  bundle=$(echo ${appImageJson} | jq -r '.name')
  if [ -z "${bundle}" ]; then
    echo
    echo "Error: Failed to detect bundle name."
    cleanup
    exit ${exitStatusFail}
  fi

  tempdlfolder=$(mktemp -d)
  cd "${tempdlfolder}"
  bundlePath=${tempdlfolder}/${bundle}

  deleteFile "${bundlePath}"

  echo "Downloading bundle: ${bundle}..."

  # see where the download is forwarded (cdn):
  local urlDownloadBundle=$(echo ${appImageJson} | jq -r '.browser_download_url')
  local fwdurl=$(getEffectiveUrl ${urlDownloadBundle})
  if [ -z "${fwdurl}" ]; then
    echo
    echo "Error: Failed to detect bundle download URL."
    cleanup
    exit ${exitStatusFail}
  fi

  echo "From: ${fwdurl}..."

  wget -qnv --show-progress -O "${bundlePath}" ${fwdurl}

  if [[ ! -f "${bundlePath}" ]]; then
    echo
    echo "Error: Failed to download bundle: ${bundlePath}."
    cleanup
    exit ${exitStatusFail}
  else
    echo " done."
  fi
}

function verifyChecksum () {
  # no checksum verification
  :
}

function deployApp () {

  echo "Cleanup destination folder before install..."
  deleteFile "${shortcutFile}"
  deleteFolder "${appInstallDir}"
  echo "...done."

  cd "${tempdlfolder}"
  echo -n "Extracting initial archive..."
  7z x "${bundlePath}" &> /dev/null
  echo " done."

  echo -n "Waiting 2 seconds..."
  sleep 2
  echo " done."

  local tarFileName=$(7z l -slt "${bundlePath}" | egrep -i "path\s+=" | tail -1 | awk -F'[= ]' '{print $4}')
  local tarFilePath=${tempdlfolder}/${tarFileName}

  if [[ ! -f "${tarFilePath}" ]]; then
    echo "Error: Failed to extract initial archive: ${bundlePath} to ${tarFilePath}."
    cleanup
    exit 1
  else
    echo "Tar file extracted: ${tarFilePath}"
  fi

  local untarredDir="${tempdlfolder}/${appname}"
  deleteFolder "${untarredDir}"
  mkdir -p "${untarredDir}"

  echo -n "Extracting tar archive..."
  tar -xf "${tarFilePath}" -C "${untarredDir}" &> /dev/null
  echo " done."

  echo -n "Waiting 2 seconds..."
  sleep 2
  echo " done."

  if [[ ! -d "${untarredDir}" ]]; then
    echo "Error: Failed to extract initial archive: ${bundlePath} to ${tarFilePath}."
    cleanup
    exit 1
  else
    echo "Tar extracted to: ${untarredDir}"
  fi

  mv "${untarredDir}" "${installPath}/"

  # mkdir -p "${appInstallDir}"

  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -ne 0 ]; then
    # failure
    # echo "Failed to create installation folder: ${appInstallDir}."
    echo "Failed to move "${untarredDir}" to "${installPath}/"
    cleanup
    exit ${exitStatusFail}
  fi
  
  # copy icon
  mkdir -p "${installIconDir}"
  deleteFile "${installIconDir}/${appname}.png"
  cp "${appInstallDir}/Resources/GitAhead.iconset/icon_512x512.png" "${installIconDir}/${appname}.png"
  echo "Copied icon file to: ${installIconDir}/${appname}.png"
}

function restoreShortcuts () {
  setSymlink "${appInstallDir}/${appname_proper}" "${shortcutFile}"
}

function deployDesktopFile () {

  deleteFile "${desktopFile}"
  echo -n "Creating desktop file: ${desktopFile}..."

  echo "[Desktop Entry]" > "${desktopFile}"
  echo "Version=1.0" >> ${desktopFile}
  echo "Encoding=UTF-8" >> "${desktopFile}"
  echo "Name=${appname_proper}" >> "${desktopFile}"
  echo "Comment=${appname_proper} Git GUI Client" >> "${desktopFile}"
  echo "GenericName=Git Client" >> "${desktopFile}"
  echo "X-GNOME-FullName=${appname_proper} Git GUI Client" >> "${desktopFile}"
  echo "Exec=${appname}" >> "${desktopFile}"
  echo "Terminal=false" >> "${desktopFile}"
  echo "Type=Application" >> "${desktopFile}"
  echo "Icon=${installIconDir}/${appname}.png" >> "${desktopFile}"
  #echo "StartupWMClass=${appname_proper}" >> "${desktopFile}"
  echo "Categories=GTK;Development;" >> "${desktopFile}"

  echo " done."
}

function uninstall () {
  echo "Uninstalling ${appname}..."
  deleteFile "${shortcutFile}"
  deleteFile "${desktopFile}"
  deleteFolder "${appInstallDir}"
  echo "...uninstalled successfully."
}

function performInstall () {

  echo "Installing latest ${appname_proper}..."

  downloadBundle
  verifyChecksum

  deployApp
  restoreShortcuts
  deployDesktopFile
}

function main () {

  # read -p "Press any key to continue: " decision

  # read -p "Stopped for debug. Continue? : [Y/n] : " decision
  # if [[ ${decision} == [nN]* ]]; then
  #     echo "Script execution stopped."
  #     exit 1
  # fi
  prepare

  checkArguments

  decisionBlock

  cleanup

  echo
  echo -n "Installation completed!"
  if [ "${systemWide}" == "true" ]; then
      echo -n " Run as normal user:"
  else
      echo -n " Run:"
  fi
  echo " >\$ ${appname} --version"
  echo

}

############
### main ###
############

main

exit ${exitStatusSuccess}
