#!/bin/bash
#
# This script will automagically download and install the yt-dlp console app
# from: https://github.com/yt-dlp/yt-dlp/releases/latest
#
# Run with sudo to install system-wide, or without to install locally.
# 1. run without any parameters to download and install with confirmation.
# 2. pass parameter: -y in order to auto-upgrade when a new version is found.
# 3. pass parameter: -x to uninstall the app
#
# Depends on: jq, sed, awk, tar, sleep, egrep, getconf, curl, wget, sha256sum, test, tr, trash-cli
#

appname=yt-dlp
appname_proper=${appname}
#appname_proper=$(echo ${appname} | sed -E 's/^(.)(.*)$/\u\1\2/')
iconName=$(echo ${appname_proper}.png | sed -E 's|^(.*)$|\L\1|')
# iconName=$(echo ${appname_proper}.png | tr '[:upper:]' '[:lower:]')

allargs=("$@")
numargs=$#

doUninstall=false
optQueryUpdate=false
optAutoUpgrade=false
exitStatusSuccess=0
exitStatusFail=1
exitStatusUpdateAvailable=100
deleteToTrash=true

urlLatest=https://api.github.com/repos/${appname}/${appname}/releases/latest
iconSize=512
iconSubFolder=${iconSize}x${iconSize} # could also be: scalable or 256x256 or 512x512

function printUsage () {
  echo
  echo "-------------------------------------------------------"
  echo "This script \"$(basename $0)\" will download and install ${appname_proper} from github."
  echo "Run with sudo to install system-wide, or without to install locally."
  echo "Parameters:"
  echo " -q : optional, exclusive, will only detect if an update is available"
  echo " -x : optional, exclusive, will uninstall the app"
  echo " -y : optional, exclusive, will auto-upgrade when a new version is found"
  echo
  echo "When no params passed, will ask for confirmation before installation."
  echo
  echo "Examples:"
  echo "\$ $(basename $0)"
  echo "\$ $(basename $0) -q"
  echo "\$ $(basename $0) -x"
  echo "\$ $(basename $0) -y"
  echo "-------------------------------------------------------"
  echo
}

function pause () {
  read -p "$*"
}

function fileExists () {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    echo "true"
  else
    echo "false"
  fi
}

function setSymlink () {
  # param 1 is "what"
  # param 2 is "where"
  # as in: ln -s what where...

  echo -n "Symlinking \"$1\" to \"$2\"..."
  rm -f "$2" &> /dev/null
  #pause "after removing $2..."
  ln -s "$1" "$2" &> /dev/null
  echo " done"
  #pause "after restoring $2 to $1..."
}

function getTrashMsg () {
  local trashMsg=
  if [[ "${deleteToTrash}" == "true" ]]; then
    trashMsg=' to trash'
  fi
  echo "${trashMsg}"
}

function deleteFile () {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    # the file or symlink exists
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -f "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function deleteFolder () {
  if [[ -d "$1" ]]; then
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -rf "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function cleanup () {
  echo "Cleanup..."
  deleteFolder "${tempdlfolder}"
  if [[ -d "${initialDir}" ]]; then
    cd "${initialDir}"
  fi
  echo "Cleanup done."
}

function getEffectiveUrl () {
  local initialUrl=$1
  # see where the url is forwarded:
  local fwdurl=$(curl -o /dev/null -w "%{url_effective}\n" -I -L -s -S ${initialUrl})
  echo "${fwdurl}"
}

function handleExitStatus () {
  # arg1: status code; arg2: message
  local EXIT_STATUS=$1
  if [ ${EXIT_STATUS} -ne 0 ]; then
    # failure
    local ERR_MSG=$2
    if [[ -z "${ERR_MSG}" ]]; then
      ERR_MSG="${appname} generic installation error."
    fi
    echo "${ERR_MSG}"
    cleanup
    echo
    exit ${exitStatusFail}
  fi
}

function downloadFile () {
  # param 1: url
  # param 2: file path (destination)

  local locUrl="$1"
  local locFilePath="$2"
  deleteFile "${locFilePath}"
  echo "Downloading file: ${locFilePath}..."
  # see where the download is forwarded (cdn):
  local fwdurl=$(getEffectiveUrl "${locUrl}")
  echo "From: ${fwdurl}..."

  wget -qnv --show-progress -O "${locFilePath}" "${fwdurl}"
  if [[ ! -f "${locFilePath}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Failed to download file: \"${locFilePath}\" from \"${locUrl}\""
  else
    echo " done."
  fi
}

function prepare () {
  systemWide=false
  tempdlfolder=/tmp/##${appname}_temp_dl##
  initialDir=${PWD}

  shortcutDir=${HOME}/.local/bin
  installPath=${HOME}/.local/lib
  installDesktopDir=${HOME}/.local/share/applications
  installIconDir=${HOME}/.local/share/icons/hicolor/${iconSubFolder}/apps

  local installVerb=install
  if [[ "${doUninstall}" == "true" ]]; then
    installVerb=uninstall
  elif [[ "${optQueryUpdate}" == "true" ]]; then
    installVerb='query version'
  fi

  # check for system-wide installation
  if [ "$(id -u)" == "0" ]; then
    systemWide=true
    deleteToTrash=false
    shortcutDir=/usr/local/bin
    installPath=/opt
    installDesktopDir=/usr/share/applications
    installIconDir=/usr/share/icons/hicolor/${iconSubFolder}/apps
    echo "Will ${installVerb} system-wide."
  else
    echo "Will ${installVerb} for current user only."
  fi

  appInstallDir=${installPath}/${appname}
  shortcutFile=${shortcutDir}/${appname}
  desktopFile=${installDesktopDir}/${appname}.desktop
  iconFile=${installIconDir}/${iconName}
}

function checkInstallPath () {
  echo -n "Checking if installation path exists: ${installPath}..."
  if [[ ! -d "${installPath}" ]]; then
    echo
    echo "Creating directory: ${installPath}."
    mkdir -p "${installPath}"
    handleExitStatus $? "Failed to create installation folder: ${installPath}."
  else
    echo " it does."
  fi
}

function detectAppVersions () {

  if [[ "${doUninstall}" != "true" ]]; then

    echo "Detecting latest release version from: ${urlLatest} ..."

    local fwdurl=$(getEffectiveUrl ${urlLatest})
    latestJson=$(curl -s -S "${fwdurl}" | jq .)

    # look for: { tag_name: 2023.09.24 }
    remotever=$(echo "${latestJson}" | jq -r .tag_name)
    localver=UNKNOWN

    # instead of: `command -v`, `which` could also work...
    if ! [[ -x "$(command -v ${appname})" ]] ; then
      echo "${appname_proper} is not currently installed"
    else
      localver=$(${appname} --version)
    fi

    echo "Remote ${appname_proper} version found: ${remotever}."
    echo "Local  ${appname_proper} version found: ${localver}."
  fi
}

function checkArguments () {

  if [ ${numargs} -gt 0 ]; then
    echo "Argument(s) passed: ${allargs[@]}."

    # uninstall is exclusive
    if [[ "${doUninstall}" == "true" ]]; then
      optAutoUpgrade=false
    fi
  fi
}

function decisionBlock () {

  # if no args, let user decide
  if [ ${numargs} -lt 1 ]; then
    read -p "Install latest version available ${remotever}? [y/N] : " decision

    if [[ ${decision} != [yY]* ]]; then
      handleExitStatus ${exitStatusFail} "Error: User denied installation. Aborting."
    fi
  else
    # some args were passed
    if [[ "${doUninstall}" == "true" ]]; then

      read -p "Uninstall ${appname}? [y/N] : " decision

      if [[ ${decision} != [yY]* ]]; then
        handleExitStatus ${exitStatusFail} "Error: User declined un-installation. Aborting."
      fi
    elif [[ "${optQueryUpdate}" == "true" ]]; then
      cleanup
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "${appname_proper} latest version ${remotever} already installed."
        exit ${exitStatusSuccess}
      fi
      echo "${appname_proper} update available: ${remotever}"
      exit ${exitStatusUpdateAvailable}
    else
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "Latest ${appname_proper} version ${remotever} already installed, nothing to do."
        cleanup
        exit ${exitStatusSuccess}
      fi

      if [[ "${optAutoUpgrade}" == "true" ]]; then
        echo "Will upgrade to version: ${remotever}..."
      fi
    fi
  fi

  if [[ "${doUninstall}" == "true" ]]; then
    uninstall
  else
    performInstall
  fi
}

function downloadBundle () {

  # local appImageJson=$(echo "${latestJson}" | jq -r '.assets[] | select(.name|test("[.]AppImage$"; "ins"))')
  # bundle=$(echo "${appImageJson}" | jq -r '.name')

  local appImageJson=$(echo "${latestJson}" | jq -r '.assets[] | select(.name=="'"${appname}"'")')
  bundle=$(echo "${appImageJson}" | jq -r '.name')
  # if we don't find then error out:
  if [ -z ${bundle} ]; then
    handleExitStatus ${exitStatusFail} "${appname} installation failed to detect bundle"
  fi

  tempdlfolder=$(mktemp -d)
  bundlePath="${tempdlfolder}/${bundle}"
  local urlDownloadBundle=$(echo "${appImageJson}" | jq -r '.browser_download_url')

  downloadFile "${urlDownloadBundle}" "${bundlePath}"

  chmod +x "${bundlePath}"
}

function getFileByName () {
  # download a file as per json
  local testexpr="$1"
  local localJson=$(echo "${latestJson}" | jq -r '.assets[] | select(.name=="'"${testexpr}"'")')
  local localFileName=$(echo "${localJson}" | jq -r '.name')
  local localFileUrl=$(echo "${localJson}" | jq -r '.browser_download_url')
  local localFilePath="${tempdlfolder}/${localFileName}"
  downloadFile "${localFileUrl}" "${localFilePath}"
}

function verifyChecksum () {
  # this works only after we've imported the gpg key:
  # curl -L https://github.com/yt-dlp/yt-dlp/raw/master/public.key | gpg --import

  local signFile=SHA2-256SUMS.sig
  local sumsFile=SHA2-256SUMS
  local signFilePath=${tempdlfolder}/${signFile}
  local sumsFilePath=${tempdlfolder}/${sumsFile}

  # download sha256sums file and signature
  getFileByName ${signFile}
  getFileByName ${sumsFile}

  # verify checksum file against signature
  echo -n "Verifying checksum file against signature file..."
  gpg --verify ${signFilePath} ${sumsFilePath} &>/dev/null
  handleExitStatus $? "Error: Failed to verify checksum file against signature file."
  echo " Pass."

  cd "${tempdlfolder}"
  echo -n "Verifying SHA256 checksum..."
  sha256sum -c --ignore-missing --status ${sumsFile} &>/dev/null
  handleExitStatus $? "Error: Checksum verification failed."
  echo " Pass."
}

function deployApp () {

  echo "Cleanup destination folder before install..."
  deleteFile "${shortcutFile}"
  deleteFolder "${appInstallDir}"
  echo "...done."

  # read -p "Stopped for debug. Continue? : [Y/n] : " decision
  # if [[ ${decision} == [nN]* ]]; then
  #     echo "Script execution stopped."
  #     exit 1
  # fi

  mkdir -p "${appInstallDir}"
  handleExitStatus $? "Failed to create installation folder: ${appInstallDir}."

  cp "${bundlePath}" "${appInstallDir}/"
  handleExitStatus $? "Failed to deploy app binary."
  echo "App binary deployed."

  chmod +x "${appInstallDir}/${bundle}"
}

function restoreShortcuts () {
  setSymlink "${appInstallDir}/${bundle}" "${shortcutFile}"
}

function deployIcons () {
  deleteFile "${iconFile}"

  local iconPartialPath="usr/share/icons/hicolor/${iconSubFolder}/apps"
  cd "${tempdlfolder}"
  # extract icon file from bundle -- 512x512 icon
  # declare var first, otherwise the return exit code will be masked:
  local tempIconPart=
  tempIconPart=$(${bundlePath} --appimage-extract "${iconPartialPath}/${iconName}")
  handleExitStatus $? "Error: Failed to extract icon file: ${iconName}."

  local tempIcon=${tempdlfolder}/${tempIconPart}
  if [[ ! -f "${tempIcon}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Extracted icon file not found: ${tempIcon}."
  else
    # copy it into final location
    echo -n "Deploying icon file: ${iconName} into ${installIconDir}/..."
    mkdir -p "${installIconDir}"
    cp "${tempIcon}" "${installIconDir}/" &> /dev/null

    if [[ ! -f "${iconFile}" ]]; then
      handleExitStatus ${exitStatusFail} "Error: Failed to copy icon file: ${iconFile}."
    fi

    echo " done."
  fi

  if [[ "${systemWide}" == "true" ]]; then
    # also copy the icon to pixmaps folder
    mkdir -p /usr/share/pixmaps/ &> /dev/null
    cp "${tempIcon}" /usr/share/pixmaps/ &> /dev/null
  fi
}

function deployDesktopFile () {

  deleteFile "${desktopFile}"

  cd "${tempdlfolder}"
  # extract desktop file from bundle
  local tempDesktopFile=${appname}.desktop
  # declare var first, otherwise the return exit code will be masked:
  local tempDesktopPart=
  tempDesktopPart=$("${bundlePath}" --appimage-extract "${tempDesktopFile}")

  handleExitStatus $? "Error: Failed to extract desktop file: ${tempDesktopFile}."

  local tempDesktop=${tempdlfolder}/${tempDesktopPart}

  if [[ ! -f "${tempDesktop}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Desktop file not found: ${tempDesktop}."
  else
    # modify the Exec line to be precise
    sed -i -E "s|^Exec=\w+(.*)$|Exec=${appname}\1|g" "${tempDesktop}"
    # copy it into final location
    echo -n "Deploying desktop file: ${desktopFile}..."
    cp "${tempDesktop}" "${installDesktopDir}/" &> /dev/null
    echo " done."
  fi
}

function uninstall () {
  echo "Uninstalling ${appname}..."
  deleteFolder "${appInstallDir}"
  deleteFile "${shortcutFile}"
  deleteFile "${desktopFile}"
  # deleteFile "${iconFile}"
  echo "...uninstalled successfully."
}

function performInstall () {

  echo "Installing latest ${appname_proper}..."

  checkInstallPath
  downloadBundle
  verifyChecksum

  deployApp
  restoreShortcuts
  # deployIcons
  # deployDesktopFile
}

function main () {

  # read -p "Press any key to continue: " decision
  prepare

  checkArguments

  detectAppVersions

  decisionBlock

  cleanup

  echo
  echo "${appname_proper} installation completed!"
  echo

}

############
### main ###
############

echo "> \"$(basename $0)\" will download and install ${appname_proper} from ${urlHome}."
# checking arguments cannot be done in a function
# list of arguments expected in the input
optstring=":qxy"
while getopts ${optstring} arg; do
  case $arg in
    q)
      optQueryUpdate=true
      echo " Will query for an available update."
      ;;
    x)
      doUninstall=true
      echo "Will uninstall ${appname}."
      ;;
    y)
      optAutoUpgrade=true
      echo "Will upgrade if any update is available."
      ;;
    *)
      echo "Unknown argument(s): -${arg}. Exiting."
      printUsage
      exit ${exitStatusFail}
      ;;
  esac
done

main

exit ${exitStatusSuccess}
